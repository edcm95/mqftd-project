package game.terceirafila.interfaces;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public interface Collidable extends Movable {

    public abstract boolean isMovingUp();

    public abstract boolean isMovingDown();

    public abstract boolean isMovingLeft();

    public abstract boolean isMovingRight();

    public abstract void setCollidingUP(boolean value);

    public abstract void setCollidingDOWN(boolean value);

    public abstract void setCollidingLEFT(boolean value);

    public abstract void setCollidingRIGHT(boolean value);

    public abstract Rectangle getHitBox();

}
