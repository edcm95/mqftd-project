package game.terceirafila;

import game.terceirafila.entities.*;
import game.terceirafila.interfaces.Collidable;
import game.terceirafila.interfaces.Movable;
import game.terceirafila.tools.CollisionEngine;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.Iterator;
import java.util.LinkedList;

public class Game implements KeyboardHandler {


    //------------------------------------------MISC PROPERTIES---------------------------------------------------------

    private Player hero;
    private boolean run;
    private boolean inStart;
    private Enemy enemy;

    //-----------------------------------------COLLISION RELATED PROPERTIES---------------------------------------------
    // TODO: 10/10/2019 Separate listOfPlayers from ListOfEnemies? Currently both in Movables

    private LinkedList<Collidable> listOfCollidables;
    private LinkedList<Movable> listOfMovables;
    private LinkedList<Bullet> listOfBullets;
    private LinkedList<Enemy> listOfEnemies;
    private CollisionEngine collisionEngine;
    private Rectangle gameField;
    private Picture field;
    private Picture hud;

    public Game() {

        this.gameField = new Rectangle(10, 10, 1024, 625);
        this.field = new Picture(10, 10, "resources/arenaTop.gif");
        this.hud = new Picture(10, 625, "resources/arenaBottom.gif");




    }

    public void init() {

        /*Write game initialization sequence
        - Must instance every object required for start()
         */

        //Initialize entities
        field.draw();
        hud.draw();
        hero = new Player(1, this);
        hero.playerDraw(200, 300);


        Enemy enemyOne = new Enemy(50, this);
        enemyOne.enemyDraw(900, 300);
        enemy = enemyOne;



        //Initialize lists
        listOfCollidables = new LinkedList<>();
        listOfMovables = new LinkedList<>();
        listOfBullets = new LinkedList<>();
        listOfEnemies = new LinkedList<>();

        //Populate lists
        listOfMovables.add(hero);
        listOfMovables.add(enemyOne);

        listOfCollidables.add(hero);
        listOfCollidables.add(enemyOne);

        listOfEnemies.add(enemyOne);

        /*System.out.println("Number of collidables: " + listOfCollidables.size());
        System.out.println("Number of movables: " + listOfMovables.size());
        System.out.println("Number of enemies: " + listOfEnemies.size());*/

        //Init collision engine
        collisionEngine = new CollisionEngine(this);

        //Final inits
        inStart = true;
        run = false;

    }

    //---------------------------------------------------GAME START-----------------------------------------------------


    public void start() throws InterruptedException {

        keyboardSetup();

        //Start menu cycle



        this.startMenu();

        while (run) {

            if(hero.getDead()){
                run = false;
                inStart = true;
                hero.setDead(true);
                startMenu();
            }

            if (enemy.isDead()) {
                run = false;
                inStart = true;
                startMenu();
            }

            //Player fire cycle
            if (hero.canFire() && hero.getFiringState()) {
                listOfBullets.add(getPlayer().fire());
            }

            //Bullet cycle
            Iterator<Bullet> bulletIterator = listOfBullets.iterator();
            while (bulletIterator.hasNext()) {

                Bullet bullet = bulletIterator.next();

                bullet.move();
                collisionEngine.checkBulletCollisions(bullet);

                if (bullet.isSpent()) {
                    bulletIterator.remove();
                }
            }


            //Move movables
            for (Movable entity : listOfMovables) {
                entity.move();
            }

            //Collisions engine cycle
            collisionEngine.availCollisions();

            //Delay
            Thread.sleep(3);
        }

        //Exit game
        System.exit(0);
    }

    //--------------------------------------------------MENU RELATED SHIT-----------------------------------------------

    public void startMenu() throws InterruptedException {
        Picture startMenu = new Picture(10, 10, "resources/startscreen.gif");
        startMenu.draw();


        while (inStart) {
            Thread.sleep(50);
        }

        startMenu.delete();
    }

    //--------------------------------------------------INPUT SETUP-----------------------------------------------------

    private void keyboardSetup() {

        Keyboard keyboard = new Keyboard(this);

        int[] keys = new int[]{
                KeyboardEvent.KEY_W,
                KeyboardEvent.KEY_A,
                KeyboardEvent.KEY_D,
                KeyboardEvent.KEY_S,
                KeyboardEvent.KEY_UP,
                KeyboardEvent.KEY_DOWN,
                KeyboardEvent.KEY_LEFT,
                KeyboardEvent.KEY_RIGHT,
                KeyboardEvent.KEY_P,
                KeyboardEvent.KEY_Q,
                KeyboardEvent.KEY_SPACE
        };


        for (int key : keys) {
            createKeyboardEvent(key, KeyboardEventType.KEY_PRESSED, keyboard);
            createKeyboardEvent(key, KeyboardEventType.KEY_RELEASED, keyboard);
        }
    }

    private void createKeyboardEvent(int key, KeyboardEventType type, Keyboard keyboard) {
        KeyboardEvent keyboardEvent = new KeyboardEvent();
        keyboardEvent.setKey(key);
        keyboardEvent.setKeyboardEventType(type);
        keyboard.addEventListener(keyboardEvent);
    }

    @Override
    public void keyPressed(KeyboardEvent e) {

        switch (e.getKey()) {

            case KeyboardEvent.KEY_UP:
                hero.setUp(true);
                hero.getActiveImage().delete();
                hero.pictureDraw("up");
                hero.setActiveImage(hero.getHeroMoveUp());
                break;

            case KeyboardEvent.KEY_DOWN:
                hero.setDown(true);
                break;

            case KeyboardEvent.KEY_LEFT:
                hero.setLeft(true);
                break;

            case KeyboardEvent.KEY_RIGHT:
                hero.setRight(true);
                break;
            //------------------------------FIRE BULLET CONTROLS--------------------------------------------------------

            case KeyboardEvent.KEY_A:
                hero.setFiringDirection(Direction.LEFT);
                hero.setFiringState(true);
                hero.getActiveImage().delete();
                hero.pictureDraw("shootLeft");
                hero.setActiveImage(hero.getHeroShootLeft());
                break;

            case KeyboardEvent.KEY_S:
                hero.setFiringDirection(Direction.DOWN);
                hero.setFiringState(true);
                hero.getActiveImage().delete();
                hero.pictureDraw("shootDown");
                hero.setActiveImage(hero.getHeroShootDown());
                break;

            case KeyboardEvent.KEY_W:
                hero.setFiringDirection(Direction.UP);
                hero.setFiringState(true);
                hero.getActiveImage().delete();
                hero.pictureDraw("shootUp");
                hero.setActiveImage(hero.getGetHeroShootUp());
                break;

            case KeyboardEvent.KEY_D:
                hero.setFiringDirection(Direction.RIGHT);
                hero.setFiringState(true);
                hero.getActiveImage().delete();
                hero.pictureDraw("shootRight");
                hero.setActiveImage(hero.getHeroShootRight());
                break;

            //--------------------------------MISC CONTROLS-------------------------------------------------------------

            case KeyboardEvent.KEY_P:
                run = false;
                break;

            case KeyboardEvent.KEY_Q:
                inStart = false;
                break;

            case KeyboardEvent.KEY_SPACE:
                run = true;
                inStart = false;
                break;

        }
    }

    @Override
    public void keyReleased(KeyboardEvent e) {

        switch (e.getKey()) {

            case KeyboardEvent.KEY_UP:
                hero.setUp(false);
                hero.getActiveImage().delete();
                hero.pictureDraw("standing");
                hero.setActiveImage(hero.getHeroStanding());
                break;

            case KeyboardEvent.KEY_DOWN:
                hero.setDown(false);
                break;

            case KeyboardEvent.KEY_LEFT:
                hero.setLeft(false);
                break;

            case KeyboardEvent.KEY_RIGHT:
                hero.setRight(false);
                break;

            //WASD RELEASE

            case KeyboardEvent.KEY_A:
                hero.setFiringState(false);
                hero.getActiveImage().delete();
                hero.pictureDraw("standing");
                hero.setActiveImage(hero.getHeroStanding());
                break;

            case KeyboardEvent.KEY_S:
                hero.setFiringState(false);
                hero.getActiveImage().delete();
                hero.pictureDraw("standing");
                hero.setActiveImage(hero.getHeroStanding());
                break;

            case KeyboardEvent.KEY_W:
                hero.setFiringState(false);
                hero.getActiveImage().delete();
                hero.pictureDraw("standing");
                hero.setActiveImage(hero.getHeroStanding());
                break;

            case KeyboardEvent.KEY_D:
                hero.setFiringState(false);
                hero.getActiveImage().delete();
                hero.pictureDraw("standing");
                hero.setActiveImage(hero.getHeroStanding());
                break;

        }
    }
    //-----------------------------------------END OF INPUT SETUP-------------------------------------------------------

    //----------------------------------------------GETTERS-------------------------------------------------------------

    public Rectangle getGameField() {
        return gameField;
    }

    public Player getPlayer() {
        return hero;
    }

    public LinkedList<Bullet> getListOfBullets() {
        return listOfBullets;
    }

    public LinkedList<Collidable> getListOfCollidables() {
        return listOfCollidables;
    }

    public LinkedList<Enemy> getListOfEnemies(){
        return listOfEnemies;
    }

    //----------------------------SETTERS----------------------------------------------

}
