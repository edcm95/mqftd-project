package game.terceirafila.tools;

import game.terceirafila.Game;
import game.terceirafila.entities.Bullet;
import game.terceirafila.entities.Enemy;
import game.terceirafila.entities.Player;
import game.terceirafila.interfaces.Collidable;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.LinkedList;

public class CollisionEngine {
    private LinkedList<Collidable> allCollidables;
    private Rectangle field;
    private Game game;

    //Collision engine only works with bullets and regular collidables

    public CollisionEngine(Game game) {
        this.game = game;
        this.field = game.getGameField();
        this.allCollidables = game.getListOfCollidables();
    }

    public void availCollisions() {

        for (int i = 0; i < allCollidables.size(); i++) {

            for (int j = 0; j < allCollidables.size(); j++) {

                if (i == j) {
                    continue;
                }

                if (allCollidables.get(i).isMovingUp()) {
                    allCollidables.get(i).setCollidingUP(areCollidingUP(allCollidables.get(i), allCollidables.get(j)));
                }

                if (allCollidables.get(i).isMovingDown()) {
                    allCollidables.get(i).setCollidingDOWN(areCollidingDOWN(allCollidables.get(i), allCollidables.get(j)));
                }

                if (allCollidables.get(i).isMovingLeft()) {
                    allCollidables.get(i).setCollidingLEFT(areCollidingLEFT(allCollidables.get(i), allCollidables.get(j)));
                }

                if (allCollidables.get(i).isMovingRight()) {
                    allCollidables.get(i).setCollidingRIGHT(areCollidingRIGHT(allCollidables.get(i), allCollidables.get(j)));
                }
            }
        }

        for(Enemy enemy : game.getListOfEnemies()){

            if(areColliding(game.getPlayer(), enemy)){

                game.getPlayer().getHit(1);
                System.out.println("Player hit!");
            }
        }

    }

    //--------------------------------------DIRECTIONAL COLLISIONS CORE-------------------------------------------------

    // TODO: 13/10/2019 make separate method for field collision........

    private boolean areCollidingUP(Collidable self, Collidable other) {

        Rectangle selfShape = self.getHitBox();
        Rectangle otherShape = other.getHitBox();

        int otherUpSide = otherShape.getY();
        int otherDownSide = otherShape.getY() + otherShape.getHeight();
        int otherRightSide = otherShape.getX() + otherShape.getWidth();
        int otherLeftSide = otherShape.getX();

        int selfUpSide = selfShape.getY();
        int selfLeftSide = selfShape.getX();
        int selfRightSide = selfShape.getX() + selfShape.getWidth();

        // Field collision UP
        if (selfUpSide <= field.getY()) {
            return true;
        }

        return selfUpSide <= otherDownSide &&
                selfUpSide > otherUpSide &&
                selfLeftSide <= otherRightSide &&
                selfRightSide >= otherLeftSide;
    }

    private boolean areCollidingDOWN(Collidable self, Collidable other) {

        Rectangle selfShape = self.getHitBox();
        Rectangle otherShape = other.getHitBox();

        int otherUpSide = otherShape.getY();
        int otherDownSide = otherShape.getY() + otherShape.getHeight();
        int otherRightSide = otherShape.getX() + otherShape.getWidth();
        int otherLeftSide = otherShape.getX();

        int selfDownSide = selfShape.getY() + selfShape.getHeight();
        int selfLeftSide = selfShape.getX();
        int selfRightSide = selfShape.getX() + selfShape.getWidth();

        //Field collision DOWN
        if (selfDownSide >= field.getY() + field.getHeight()) {
            return true;
        }

        return selfDownSide >= otherUpSide &&
                selfDownSide < otherDownSide &&
                selfLeftSide <= otherRightSide &&
                selfRightSide >= otherLeftSide;
    }

    private boolean areCollidingLEFT(Collidable self, Collidable other) {

        Rectangle selfShape = self.getHitBox();
        Rectangle otherShape = other.getHitBox();

        int otherUpSide = otherShape.getY();
        int otherDownSide = otherShape.getY() + otherShape.getHeight();
        int otherLeftSide = otherShape.getX();
        int otherRightSide = otherShape.getX() + otherShape.getWidth();

        int selfLeftSide = selfShape.getX();
        int selfUpSide = selfShape.getY();
        int selfDownSide = selfShape.getY() + selfShape.getHeight();

        //Field collision LEFT
        if (selfLeftSide <= field.getX()) {
            return true;
        }

        return selfLeftSide <= otherRightSide &&
                selfLeftSide > otherLeftSide &&
                selfUpSide <= otherDownSide &&
                selfDownSide >= otherUpSide;
    }

    private boolean areCollidingRIGHT(Collidable self, Collidable other) {

        Rectangle selfShape = self.getHitBox();
        Rectangle otherShape = other.getHitBox();


        int otherRightSide = otherShape.getX() + otherShape.getWidth();
        int otherLeftSide = otherShape.getX();
        int otherUpSide = otherShape.getY();
        int otherDownSide = otherShape.getY() + otherShape.getHeight();

        int selfRightSide = selfShape.getX() + selfShape.getWidth();
        int selfUpSide = selfShape.getY();
        int selfDownSide = selfShape.getY() + selfShape.getHeight();

        //Field collision RIGHT
        if (selfRightSide >= field.getX() + field.getWidth()) {
            return true;
        }

        return selfRightSide >= otherLeftSide &&
                selfRightSide < otherRightSide &&
                selfUpSide <= otherDownSide &&
                selfDownSide >= otherUpSide;
    }

    //-----------------------------------------------BLIND COLLISIONS---------------------------------------------------

    // TODO: 11/10/2019 verify bullet collision mechanics

    private boolean areColliding(Collidable self, Collidable other) {
        Rectangle selfShape = self.getHitBox();
        Rectangle otherShape = other.getHitBox();

        int otherRightSide = otherShape.getX() + otherShape.getWidth();
        int otherLeftSide = otherShape.getX();
        int otherUpSide = otherShape.getY();
        int otherDownSide = otherShape.getY() + otherShape.getHeight();

        int selfRightSide = selfShape.getX() + selfShape.getWidth();
        int selfUpSide = selfShape.getY();
        int selfDownSide = selfShape.getY() + selfShape.getHeight();
        int selfLeftSide = selfShape.getX();

        return selfLeftSide < otherRightSide &&
                selfRightSide > otherLeftSide &&
                selfUpSide < otherDownSide &&
                selfDownSide > otherUpSide;
    }

    public void checkBulletCollisions(Bullet bullet) {
        for (Collidable collidable : allCollidables) {

            if(collidingField(bullet)){
                bullet.delete();
            }

            if (collidable instanceof Player) {
                continue;
            }

            if (areColliding(bullet, collidable)) {
                Enemy enemy = (Enemy) collidable;
                System.out.println("Enemy hit!");
                enemy.getHit(1);
                bullet.delete();
            }
        }
    }

    public boolean collidingField(Collidable self){
        Rectangle selfShape = self.getHitBox();

        int selfLeftSide = selfShape.getX();
        int selfRightSide = selfShape.getX() + selfShape.getWidth();
        int selfUpSide = selfShape.getY();
        int selfDownSide = selfShape.getY() + selfShape.getHeight();

        // Field collision UP
        if (selfUpSide <= field.getY()) {
            return true;
        }

        //Field collision DOWN
        if (selfDownSide >= field.getY() + field.getHeight()) {
            return true;
        }

        //Field collision LEFT
        if (selfLeftSide <= field.getX()) {
            return true;
        }

        //Field collision RIGHT
        return selfRightSide >= field.getX() + field.getWidth();
    }
}
