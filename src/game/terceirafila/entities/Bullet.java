package game.terceirafila.entities;

import game.terceirafila.interfaces.Collidable;
import game.terceirafila.interfaces.Movable;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Bullet implements Movable, Collidable {

    private Direction direction;
    private boolean spent;
    private Rectangle bulletHitBox;
    private Picture bullet;


    public Bullet(int x, int y, Direction direction) {
        bulletHitBox = new Rectangle(x, y, 10, 10);
        this.bullet = new Picture(x,y,"resources/bullet.gif");
        this.direction = direction;
    }

    public void move() {
        if (spent) {
            return;
        }

        switch (this.direction) {
            case UP:
                bulletHitBox.translate(0, -3);
                bullet.translate(0,-3);
                break;

            case DOWN:
                bulletHitBox.translate(0, 3);
                bullet.translate(0,3);
                break;

            case RIGHT:
                bulletHitBox.translate(3, 0);
                bullet.translate(3,0);
                break;

            case LEFT:
                bulletHitBox.translate(-3, 0);
                bullet.translate(-3,0);
                break;
        }

    }

    public void draw() {
        bullet.draw();
    }

    public void delete() {
        spent = true;
        bulletHitBox.delete();
        bullet.delete();
    }

    public void resetBullet(int X, int Y, Direction direction) {
        bulletHitBox.delete();
        bulletHitBox = new Rectangle(X, Y, 10, 10);
        this.direction = direction;
        this.draw();
        spent = false;
    }

    public boolean isSpent() {
        return spent;
    }


    //------------------------------------COLLISION METHODS-------------------------------------------------------------

    @Override
    public boolean isMovingUp() {
        return direction == Direction.UP;
    }

    @Override
    public boolean isMovingDown() {
        return direction == Direction.DOWN;
    }

    @Override
    public boolean isMovingLeft() {
        return direction == Direction.LEFT;
    }

    @Override
    public boolean isMovingRight() {
        return direction == Direction.RIGHT;
    }

    @Override
    public void setCollidingUP(boolean value) {

    }

    @Override
    public void setCollidingDOWN(boolean value) {

    }

    @Override
    public void setCollidingLEFT(boolean value) {

    }

    @Override
    public void setCollidingRIGHT(boolean value) {

    }

    @Override
    public Rectangle getHitBox() {
        return bulletHitBox;
    }

    //------------------------------------------------------------------------------------------------------------------
}
