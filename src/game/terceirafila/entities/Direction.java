package game.terceirafila.entities;

public enum Direction {

    UP,
    DOWN,
    LEFT,
    RIGHT
}
